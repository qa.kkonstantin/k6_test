import { check } from 'k6'

export { check_status }

function check_status (response, status_code = 200) {
  return check(response, {
    [`status code is ${status_code}`]: response =>
      response.status === status_code
  })
}
