// Способы запуска скрипта с отчётом:
// k6 run -o cloud=test-script scenario2.js
// k6 run --out web-dashboard=export=report_per_vu.html tests/public_list_cz_filtration.js
// docker run -it -p 5665:5665 -v ${pwd}:/src ghcr.io/grafana/xk6-dashboard:latest run --out web-dashboard=period=2s /src/scenario2.js
// https://github.com/grafana/xk6-dashboard
// https://qarocks.ru/performance-testing-k6/
// httpDebug: full or ''

// Определенное количество ВУ выполняет фиксированное количество общих итераций, количество итераций на 1 ВУ не имеет значения.
// Если важно время выполнения нескольких итераций теста, этот вариант подходит лучше всего.
// https://k6.io/docs/using-k6/scenarios/executors/shared-iterations/
// k6 run --out web-dashboard=export=report/report_shared_iter.html tests/public_list_cz_pagination.js
export const option = {
  httpDebug: '',
  thresholds: {
    http_req_duration: [{ threshold: 'p(95) < 5000', abortOnFail: false }],
    http_req_failed: [{ threshold: 'rate < 0.2', abortOnFail: false }]
  },
  scenarios: {
    contacts: {
      executor: 'shared-iterations',
      vus: 50,
      iterations: 25000
    }
  }
}

// Каждый ВУ выполняет точное количество итераций. Общее количество выполненных итераций равно ВУ * кол-во итераций
// Если нужно определенное количество ВУ для выполнения определённого количества итераций
// https://k6.io/docs/using-k6/scenarios/executors/per-vu-iterations/
// k6 run --out web-dashboard=export=report/report_per_vu.html tests/public_list_cz_pagination.js
// export const option = {
//   httpDebug: '',
//   thresholds: {
//     http_req_duration: [{ threshold: 'p(95) < 5000', abortOnFail: false }],
//     http_req_failed: [{ threshold: 'rate < 0.2', abortOnFail: false }]
//   },
//   scenarios: {
//     contacts: {
//       executor: 'per-vu-iterations',
//       vus: 50,
//       iterations: 50,
//       // maxDuration: '2s'
//     }
//   }
// }

// // Фиксированное количество ВУ выполняет максимально возможное количество итераций за указанное время.
// // Если нужно, чтобы определенное количество ВУ работало в течение определенного периода времени
// // https://k6.io/docs/using-k6/scenarios/executors/constant-vus/
// export const option = {
//   httpDebug: '',
//   thresholds: {
//     http_req_duration: [{ threshold: 'p(95) < 5000', abortOnFail: false }],
//     http_req_failed: [{ threshold: 'rate < 0.2', abortOnFail: false }]
//   },
//   scenarios: {
//     contacts: {
//       executor: 'constant-vus',
//       vus: 10,
//       duration: '30s'
//     }
//   }
// }

// // Переменное количество ВУ выполняет максимально возможное количество итераций за указанное время.
// // Если нужно, чтобы ВУ увеличивались или уменьшались в определенные периоды времени.
// // https://k6.io/docs/using-k6/scenarios/executors/ramping-vus/
// export const option = {
//   httpDebug: '',
//   thresholds: {
//     http_req_duration: [{ threshold: 'p(95) < 5000', abortOnFail: false }],
//     http_req_failed: [{ threshold: 'rate < 0.2', abortOnFail: false }]
//   },
//   scenarios: {
//     contacts: {
//       executor: 'ramping-vus',
//       startVUs: 0,
//       stages: [
//         { duration: '10s', target: 10 },
//         { duration: '30s', target: 0 }
//       ],
//       gracefulRampDown: '0s'
//     }
//   }
// }

// // Постоянная частота 30 итераций в секунду в течение 30 секунд. 50 ВУ для динамического использования по мере необходимости.
// // Если нужно, чтобы итерации оставались постоянными, независимо от производительности тестируемой системы.
// // Этот подход полезен, например, для более точного представления RPS
// // https://k6.io/docs/using-k6/scenarios/executors/constant-arrival-rate/
// export const option = {
//   httpDebug: '',
//   thresholds: {
//     http_req_duration: [{ threshold: 'p(95) < 5000', abortOnFail: false }],
//     http_req_failed: [{ threshold: 'rate < 0.2', abortOnFail: false }]
//   },
//   scenarios: {
//     contacts: {
//       executor: 'constant-arrival-rate',
//       // How long the test lasts
//       duration: '30s',
//       // How many iterations per timeUnit
//       rate: 30,
//       // Start `rate` iterations per second
//       timeUnit: '1s',
//       // Pre-allocate VUs
//       preAllocatedVUs: 50
//     }
//   }
// }

// // начинается с определенногоstartRate, 300 итераций в минуту в течение одной минуты.
// // Через одну минуту частота итераций увеличивается до 600 итераций в минуту в течение следующих двух минут и остается на этом уровне еще четыре минуты.
// // За последние две минуты скорость снижается до целевых 60 итераций в минуту.
// // Если нужно запускать итерации независимо от производительности тестируемой системы
// // и планируется увеличивать или уменьшать количество итераций в течение определенных периодов времени.
// // https://k6.io/docs/using-k6/scenarios/executors/ramping-arrival-rate/
// export const option = {
//   httpDebug: '',
//   thresholds: {
//     http_req_duration: [{ threshold: 'p(95) < 5000', abortOnFail: false }],
//     http_req_failed: [{ threshold: 'rate < 0.2', abortOnFail: false }]
//   },
//   scenarios: {
//     contacts: {
//       executor: 'ramping-arrival-rate',

//       // Start iterations per `timeUnit`
//       startRate: 300,

//       // Start `startRate` iterations per minute
//       timeUnit: '1m',

//       // Pre-allocate necessary VUs.
//       preAllocatedVUs: 50,
//       stages: [
//         // Start 300 iterations per `timeUnit` for the first minute.
//         { target: 300, duration: '1m' },

//         // Linearly ramp-up to starting 600 iterations per `timeUnit` over the following two minutes.
//         { target: 600, duration: '2m' },

//         // Continue starting 600 iterations per `timeUnit` for the following four minutes.
//         { target: 600, duration: '4m' },

//         // Linearly ramp-down to starting 60 iterations per `timeUnit` over the last two minutes.
//         { target: 60, duration: '2m' }
//       ]
//     }
//   }
// }
