import http from 'k6/http'
import { sleep, group } from 'k6'
import { option } from '../options.js'
import { host, timeout } from '../global_var.js'
import { check_status } from '../assertions.js'

export const options = option

const url = host
var page = 1

export function setup () {
  group('Система получает фильтры Витрины ЦЗ', function () {
    const response = http.get(`${url}/api/v1/public/quote-requests/filters`)
    check_status(response)
  })

  return group(
    `По умолчанию система открывает список ЦЗ на странице ${page}`,
    function () {
      const resp = http.get(`${url}/api/v1/public/quote-requests`)
      check_status(resp)

      return resp.headers['X-Pagination-Page-Count']
    }
  )
}

export default function public_list_cz_pagination (max_page) {
  group('Пагинация', function () {
    var page = get_random_page(max_page)
    group(`Система открывает список ЦЗ на случайной странице`, function () {
      const response = http.get(
        `${url}/api/v1/public/quote-requests?page=${page}&perPage=10&orderBy%5BupdatedAt%5D=DESC`
      )
      check_status(response)
    })
  })

  sleep(timeout)
}

export function get_random_page (max_page) {
  return Math.floor(Math.random() * max_page + 1)
}
