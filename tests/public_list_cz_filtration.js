import http from 'k6/http'
import { sleep, group } from 'k6'
import { option } from '../options.js'
import { host, timeout } from '../global_var.js'
import { check_status } from '../assertions.js'
import { randomItem } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js'

export const options = option

const url = host

export function setup () {
  return group('Система получает фильтры Витрины ЦЗ', function () {
    const response = http.get(`${url}/api/v1/public/quote-requests/filters`)
    let resp_json = response.json()
    check_status(response)

    let orderBy = ['DESC', 'ASC']
    let organizations = []

    for (let i = 0; i < resp_json.organizations.length; i++) {
      if (resp_json.organizations[i].quantity >= 90) {
        organizations.push(resp_json.organizations[i].id)
      }
    }

    let purchase_order_status = generate_param_by_filter(
      resp_json.purchaseOrderStatus,
      40
    )
    let quote_request_status = generate_param_by_filter(
      resp_json.quoteRequestStatus,
      100
    )

    return {
      organizations,
      purchase_order_status,
      quote_request_status,
      orderBy
    }
  })
}

export default function public_list_cz_filtration (params) {
  group('Фильтрация', function () {
    group(`Система осуществляет фильтрацию ЦЗ на витрине`, function () {
      const response = http.get(
        `${url}/api/v1/public/quote-requests?page=1&perPage=10&quoteRequestStatus%5B0%5D=${randomItem(
          params.quote_request_status
        )}&purchaseOrderStatus%5B0%5D=${randomItem(
          params.purchase_order_status
        )}&organizations%5B0%5D=${randomItem(
          params.organizations
        )}&orderBy%5BupdatedAt%5D=${randomItem(params.orderBy)}`
      )
      
      check_status(response)
    })
  })

  sleep(timeout)
}

function generate_param_by_filter (param, count_condition = 0, path) {
  let temp = []
  for (let i = 0; i < param.length; i++) {
    if (param[i].quantity >= count_condition) {
      temp.push(param[i].code)
    }
  }

  return temp
}
