import http from 'k6/http'
import { sleep, check, group } from 'k6'
import {
  randomString,
  randomItem
} from 'https://jslib.k6.io/k6-utils/1.2.0/index.js'
import { option } from '../options.js'
import { host, timeout } from '../global_var.js'
import { check_status } from '../assertions.js'

export const options = option

// const config = JSON.parse(open('env/' + __ENV.ENVIRONMENT + '.env.json'))
const config = JSON.parse(open('env/multi.env.json'))
const auth_url =
  'https://' + config.kk_url + '/auth/realms/master/consents/token'
const login_data = config.user
const x_auth_org = config.x_auth_org
const header = config.headers

const url = host

export function setup () {
  var resp = http.post(auth_url, login_data, header)
  var token = JSON.parse(resp.body).access_token

  var auth_headers = {
    Authorization: `Bearer ${token}`,
    'X-AUTH-ORG': x_auth_org
  }

  return { headers: auth_headers }
}

export default function create_draft_cz (auth_setup) {
  var cz_id = null
  group('Создание ЦЗ в статусе Черновик с полным набором полей', function () {
    group('Система запрашивает настройки секции', function () {
      const response = http.get(`${url}/api/v1/company/setting`, {
        headers: auth_setup.headers
      })
      check_status(response)
    })
    group(' Система запрашивает закрытые списки организации', function () {
      const response = http.get(`${url}/api/v1/company/verified-list`, {
        headers: auth_setup.headers
      })

      check_status(response)
    })
    group(
      'Заказчик сохраняет Черновик Ценового заказа с заполнением всех полей',
      function () {
        var bool_arr = [true, false]
        var purchaseType = ['com', 'fz44', 'fz223']
        const delivery_dateFrom = new Date(
          new Date().setDate(new Date().getDate() + 5)
        )
          .toISOString()
          .slice(0, 19)
        const responseDate = new Date(
          new Date().setDate(new Date().getDate() + 1)
        )
          .toISOString()
          .slice(0, 19)

        const payload = JSON.stringify({
          name: 'Ценовой запрос Черновик',
          delivery: {
            dateFrom: `${delivery_dateFrom}+03:00`,
            terms: `${randomString(10)}`,
            address: '125635, г Москва, р-н Западное Дегунино, ул Новая, д 1',
            costType: true,
            method: 'general'
          },
          requirements: {
            smallBusiness: randomItem(bool_arr),
            authorizedDealer: randomItem(bool_arr),
            russianProduction: randomItem(bool_arr),
            privateRequest: randomItem(bool_arr),
            documentsRequest: randomItem(bool_arr),
            analoguesAccept: randomItem(bool_arr),
            startMaxPriceOfContract: randomItem(bool_arr),
            verifiedListOnly: true
          },
          purchaseType: randomItem(purchaseType),
          isExpress: false,
          responseDate: `${responseDate}+03:00`,
          comment: `${randomString(30)}`,
          verifiedList: [
            {
              name: 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "БЬЮТИ 358"',
              inn: '7728328470',
              kpp: '773301001',
              ogrn: '1167746086111',
              type: 'legalEntity'
            }
          ]
        })

        const response = http.post(`${url}/api/v1/quote-requests`, payload, {
          headers: auth_setup.headers
        })

        check(response, {
          'status code is 201': response => response.status === 201,
          'response contains cz_id': response => response.body.includes('id')
        })

        cz_id = response.json().id
      }
    )
    group('Система запрашивает информацию по созданному ЦЗ', function () {
      if (cz_id != null) {
        const response = http.get(`${url}/api/v1/quote-requests/${cz_id}`, {
          headers: auth_setup.headers
        })

        check(response, {
          'status code is 200': response => response.status === 200,
          'cz_id is correct': response => response.json().id === cz_id
        })
      }
    })
    sleep(timeout)
  })
}
